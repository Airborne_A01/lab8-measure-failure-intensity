# Lab8 - measure failure intensity

## Homework

As a homework you will need to open this [link](https://docs.google.com/spreadsheets/d/1lLigF6um8pKH2IGZNt9xXcl8quPtFPzmVtt5RCLeCqw/edit?usp=sharing), load test your service(GetSpec request) and provide the results of your test as a screenshot when failure intensity is equal to the `0.2+-5%`. + provide the calculations for the failure intensity

## Results

Parameters to tune using Apache Benchmark are number of requests (-n) and concurrency level (-c). Assuming that failure intensity rate is constant, the amount is not really important, but it should be set so that the measure will be precise. 

Multiple measures were taken and seems that the service at the time of testing was not really stable.

The best result is:

FI = (2 failures)/(106.277 seconds) = 0.18

for 100 reqeusts with concurency level 1

![](screenshot.png)

### Measures

> number of requests, concurrency level, failure intensity

#### 100, 5, FI=0
```
Concurrency Level:      5
Time taken for tests:   18.532 seconds
Complete requests:      100
Failed requests:        0
Total transferred:      122800 bytes
HTML transferred:       68500 bytes
Requests per second:    5.40 [#/sec] (mean)
Time per request:       926.601 [ms] (mean)
Time per request:       185.320 [ms] (mean, across all concurrent requests)
Transfer rate:          6.47 [Kbytes/sec] received
```

#### 500, 25, FI=2.31
```
Concurrency Level:      25
Time taken for tests:   37.916 seconds
Complete requests:      500
Failed requests:        88
   (Connect: 0, Receive: 0, Length: 88, Exceptions: 0)
Total transferred:      612468 bytes
HTML transferred:       340968 bytes
Requests per second:    13.19 [#/sec] (mean)
Time per request:       1895.794 [ms] (mean)
Time per request:       75.832 [ms] (mean, across all concurrent requests)
Transfer rate:          15.77 [Kbytes/sec] received
```

#### 500, 15 FI=2.16
```
Concurrency Level:      15
Time taken for tests:   50.390 seconds
Complete requests:      500
Failed requests:        108
   (Connect: 0, Receive: 0, Length: 108, Exceptions: 0)
Total transferred:      612264 bytes
HTML transferred:       340764 bytes
Requests per second:    9.92 [#/sec] (mean)
Time per request:       1511.688 [ms] (mean)
Time per request:       100.779 [ms] (mean, across all concurrent requests)
Transfer rate:          11.87 [Kbytes/sec] received
```

#### 500, 12 FI=1.42  
```
Concurrency Level:      12
Time taken for tests:   60.861 seconds
Complete requests:      500
Failed requests:        87
   (Connect: 0, Receive: 0, Length: 87, Exceptions: 0)
Total transferred:      614943 bytes
HTML transferred:       343443 bytes
Requests per second:    8.22 [#/sec] (mean)
Time per request:       1460.666 [ms] (mean)
Time per request:       121.722 [ms] (mean, across all concurrent requests)
Transfer rate:          9.87 [Kbytes/sec] received
```

#### 500, 10 FI=7.19 (???)
```
Concurrency Level:      10
Time taken for tests:   46.762 seconds
Complete requests:      500
Failed requests:        330
   (Connect: 0, Receive: 0, Length: 330, Exceptions: 0)
Non-2xx responses:      1
Total transferred:      615804 bytes
HTML transferred:       344285 bytes
Requests per second:    10.69 [#/sec] (mean)
Time per request:       935.243 [ms] (mean)
Time per request:       93.524 [ms] (mean, across all concurrent requests)
Transfer rate:          12.86 [Kbytes/sec] received
```

#### 100, 5, FI=2,57
```
Concurrency Level:      5
Time taken for tests:   33.964 seconds
Complete requests:      100
Failed requests:        85
   (Connect: 0, Receive: 0, Length: 85, Exceptions: 0)
Total transferred:      123070 bytes
HTML transferred:       68770 bytes
Requests per second:    2.94 [#/sec] (mean)
Time per request:       1698.194 [ms] (mean)
Time per request:       339.639 [ms] (mean, across all concurrent requests)
Transfer rate:          3.54 [Kbytes/sec] received
```

#### 100, 2, FI=0.55
```
Concurrency Level:      2
Time taken for tests:   60.271 seconds
Complete requests:      100
Failed requests:        33
   (Connect: 0, Receive: 0, Length: 33, Exceptions: 0)
Total transferred:      123711 bytes
HTML transferred:       69411 bytes
Requests per second:    1.66 [#/sec] (mean)
Time per request:       1205.427 [ms] (mean)
Time per request:       602.713 [ms] (mean, across all concurrent requests)
Transfer rate:          2.00 [Kbytes/sec] received
```

#### 100, 1, FI=0.6
```
Concurrency Level:      1
Time taken for tests:   106.400 seconds
Complete requests:      100
Failed requests:        64
   (Connect: 0, Receive: 0, Length: 64, Exceptions: 0)
Total transferred:      122564 bytes
HTML transferred:       68264 bytes
Requests per second:    0.94 [#/sec] (mean)
Time per request:       1064.002 [ms] (mean)
Time per request:       1064.002 [ms] (mean, across all concurrent requests)
Transfer rate:          1.12 [Kbytes/sec] received
```

#### 100, 1 (one more time), FI=0.5
```
Concurrency Level:      1
Time taken for tests:   133.202 seconds
Complete requests:      100
Failed requests:        67
   (Connect: 0, Receive: 0, Length: 67, Exceptions: 0)
Total transferred:      122386 bytes
HTML transferred:       68086 bytes
Requests per second:    0.75 [#/sec] (mean)
Time per request:       1332.017 [ms] (mean)
Time per request:       1332.017 [ms] (mean, across all concurrent requests)
Transfer rate:          0.90 [Kbytes/sec] received
```
#### 100, 1 (one more time), FI=0.18
```
Concurrency Level:      1
Time taken for tests:   106.277 seconds
Complete requests:      100
Failed requests:        2
   (Connect: 0, Receive: 0, Length: 2, Exceptions: 0)
Total transferred:      122738 bytes
HTML transferred:       68438 bytes
Requests per second:    0.94 [#/sec] (mean)
Time per request:       1062.771 [ms] (mean)
Time per request:       1062.771 [ms] (mean, across all concurrent requests)
Transfer rate:          1.13 [Kbytes/sec] received
```